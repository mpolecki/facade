# Wzorzec Adapter

## 1. Zastosowanie
Wzorzec używa się w celu ukrywania skomplikowanych szczegółów przed użytkownikiem. Zamiast pisać wiele szczegółowych operacji, zaszywamy wszystko w jedną funkcję, która jest jasna i czytelna.

## 2. Przykład użycia
- Inicjalizacja czujnika akcelerometru może składać się przykładowo z ustawienia prędkości odczytu, włączenia 3 osi oraz włączenia samego sensora. Czytelniej jest stworzyć jedną funkcję, która to wykonuje:
	```c
	void accelerometer_sensor_init(void)
	{
		//setting measuring speed
		accelerometer_set_data_rate(FAST_DATA_RATE);

		//enable all axes
		accelerometer_enable_axis(X_AXIS, true);
		accelerometer_enable_axis(Y_AXIS, true);
		accelerometer_enable_axis(Z_AXIS, true);

		//turn on sensor
		accelerometer_set_power_mode(NORMAL_MODE);
	}
	```

- Chcemy dowiedzieć się w jakim położeniu jest czujnik względem konkretnej osi. Należy wpierw odczytać 3 osie oraz przeliczyć to za pomocą arcus tangens. Można to zamknąć do jednej funkcji podając jedynie interesującą nas płaszczyznę.
	```c
	float accelerometer_sensor_read_angle(axis_number_t axis)
	{
		float result = 0;
		char * axis_string[10];

		//read all axes
		uint8_t x_low = accelerometer_read_register(X_AXIS_REGISTER_LOW);
		uint8_t x_high = accelerometer_read_register(X_AXIS_REGISTER_HIGH);
		uint8_t y_low = accelerometer_read_register(Y_AXIS_REGISTER_LOW);
		uint8_t y_high = accelerometer_read_register(Y_AXIS_REGISTER_HIGH);
		uint8_t z_low = accelerometer_read_register(Z_AXIS_REGISTER_LOW);
		uint8_t z_high = accelerometer_read_register(Z_AXIS_REGISTER_HIGH);

		uint16_t x_value = ((uint16_t)x_high << 8) | (uint16_t)(x_low);
		uint16_t y_value = ((uint16_t)y_high << 8) | (uint16_t)(y_low);
		uint16_t z_value = ((uint16_t)z_high << 8) | (uint16_t)(z_low);

		//choose one of them for calculation
		switch(axis)
		{
			case X_AXIS:
				strcpy((char*)&axis_string, "X");
				result = atan2(y_value, z_value);
				break;

			case Y_AXIS:
				strcpy((char*)&axis_string, "Y");
				result = atan2(x_value, z_value);
				break;

			case Z_AXIS:
				strcpy((char*)&axis_string, "Z");
				result = atan2(y_value, x_value);
				break;

			default:
				break;
		}

		//print result
		result = result * RAD_TO_DEG;
		printf("Angle in %s axis is %.2f\r\n", (char*)&axis_string, result);

		return result;
	}
	```

## 3. Przebieg programu:

Inicjalizujemy czujnik, a następnie cyklicznie odczytujemy wszystkie 3 kąty położenia:
```c
  accelerometer_sensor_init();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  accelerometer_sensor_read_angle(X_AXIS);
	  accelerometer_sensor_read_angle(Y_AXIS);
	  accelerometer_sensor_read_angle(Z_AXIS);
	  HAL_Delay(1000);
  }
```
