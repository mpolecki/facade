/*
 * accelerometer.h
 *
 *  Created on: May 23, 2023
 *      Author: polecki
 */

#ifndef INC_ACCELEROMETER_HIGH_LEVEL_H_
#define INC_ACCELEROMETER_HIGH_LEVEL_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

typedef enum
{
	SLOW_DATA_RATE,
	FAST_DATA_RATE,
	NORMAL_DATA_RATE
}data_rate_t;

typedef enum
{
	X_AXIS,
	Y_AXIS,
	Z_AXIS
}axis_number_t;

typedef enum
{
	NORMAL_MODE,
	LOW_POWER_MODE,
	POWER_DOWN_MODE
}power_mode_t;

#define DATA_RATE_REGISTER (0x01)
#define AXES_REGISTER (0x02)
#define POWER_MODE_REGISTER (0x03)

#define X_AXIS_REGISTER_LOW (0x04)
#define X_AXIS_REGISTER_HIGH (0x05)

#define Y_AXIS_REGISTER_LOW (0x06)
#define Y_AXIS_REGISTER_HIGH (0x07)

#define Z_AXIS_REGISTER_LOW (0x08)
#define Z_AXIS_REGISTER_HIGH (0x09)


#define DATA_RATE_FAST_VALUE (0x01)
#define DATA_RATE_NORMAL_VALUE (0x02)
#define DATA_RATE_SLOW_VALUE (0x04)

#define X_AXIS_VALUE (0x01)
#define Y_AXIS_VALUE (0x02)
#define Z_AXIS_VALUE (0x04)

#define NORMAL_MODE_VALUE (0x01)
#define LOW_POWER_VALUE (0x02)
#define POWER_DOWN_VALUE (0x04)

void accelerometer_set_data_rate(data_rate_t data_rate);
void accelerometer_enable_axis(axis_number_t axis, bool enable);
void accelerometer_set_power_mode(power_mode_t power_mode);
void accelerometer_sensor_init(void);
uint8_t accelerometer_read_register(uint8_t register_number);
float accelerometer_sensor_read_angle(axis_number_t angle);

#endif /* INC_ACCELEROMETER_HIGH_LEVEL_H_ */
