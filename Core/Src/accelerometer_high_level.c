/*
 * accelerometer.c
 *
 *  Created on: May 23, 2023
 *      Author: polecki
 */

#include "accelerometer_high_level.h"
#include "math.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define RAD_TO_DEG 57.2957795131

static void accelerometer_write_register(uint8_t register_number, uint8_t register_value)
{
	printf("Writing value %u to register %u\r\n", register_value, register_number);
}

void accelerometer_set_data_rate(data_rate_t data_rate)
{
	uint8_t register_value = 0x00;

	if (SLOW_DATA_RATE == data_rate)
	{
		register_value = DATA_RATE_SLOW_VALUE;
	}
	else if (NORMAL_DATA_RATE == data_rate)
	{
		register_value = DATA_RATE_NORMAL_VALUE;
	}
	else if (FAST_DATA_RATE == data_rate)
	{
		register_value = DATA_RATE_FAST_VALUE;
	}

	accelerometer_write_register(DATA_RATE_REGISTER, register_value);
}

void accelerometer_enable_axis(axis_number_t axis, bool enable)
{
	uint8_t register_value = 0x00;

	switch(axis)
	{
	case X_AXIS:
		register_value = X_AXIS_VALUE;
	break;

	case Y_AXIS:
		register_value = Y_AXIS_VALUE;
	break;

	case Z_AXIS:
		register_value = Z_AXIS_VALUE;
	break;

	default:
		break;
	}

	accelerometer_write_register(AXES_REGISTER, register_value);
}

void accelerometer_set_power_mode(power_mode_t power_mode)
{
	uint8_t register_value = 0x00;

	switch(power_mode)
	{
	case NORMAL_MODE:
		register_value = NORMAL_MODE_VALUE;
		break;

	case LOW_POWER_MODE:
		register_value = LOW_POWER_VALUE;
		break;

	case POWER_DOWN_MODE:
		register_value = POWER_DOWN_VALUE;
		break;
	}

	accelerometer_write_register(POWER_MODE_REGISTER, register_value);
}

void accelerometer_sensor_init(void)
{
	//setting measuring speed
	accelerometer_set_data_rate(FAST_DATA_RATE);

	//enable all axes
	accelerometer_enable_axis(X_AXIS, true);
	accelerometer_enable_axis(Y_AXIS, true);
	accelerometer_enable_axis(Z_AXIS, true);

	//turn on sensor
	accelerometer_set_power_mode(NORMAL_MODE);
}

uint8_t accelerometer_read_register(uint8_t register_number)
{
	uint8_t result = (uint8_t)(rand() % (255 - 0 + 1) + 0);

	printf("Reading from register %u, value of %u\r\n", register_number, result);

	return result;
}

float accelerometer_sensor_read_angle(axis_number_t axis)
{
	float result = 0;
	char * axis_string[10];

	//read all axes
	uint8_t x_low = accelerometer_read_register(X_AXIS_REGISTER_LOW);
	uint8_t x_high = accelerometer_read_register(X_AXIS_REGISTER_HIGH);
	uint8_t y_low = accelerometer_read_register(Y_AXIS_REGISTER_LOW);
	uint8_t y_high = accelerometer_read_register(Y_AXIS_REGISTER_HIGH);
	uint8_t z_low = accelerometer_read_register(Z_AXIS_REGISTER_LOW);
	uint8_t z_high = accelerometer_read_register(Z_AXIS_REGISTER_HIGH);

	uint16_t x_value = ((uint16_t)x_high << 8) | (uint16_t)(x_low);
	uint16_t y_value = ((uint16_t)y_high << 8) | (uint16_t)(y_low);
	uint16_t z_value = ((uint16_t)z_high << 8) | (uint16_t)(z_low);

	//choose one of them for calculation
	switch(axis)
	{
		case X_AXIS:
			strcpy((char*)&axis_string, "X");
			result = atan2(y_value, z_value);
			break;

		case Y_AXIS:
			strcpy((char*)&axis_string, "Y");
			result = atan2(x_value, z_value);
			break;

		case Z_AXIS:
			strcpy((char*)&axis_string, "Z");
			result = atan2(y_value, x_value);
			break;

		default:
			break;
	}

	//print result
	result = result * RAD_TO_DEG;
	printf("Angle in %s axis is %.2f\r\n", (char*)&axis_string, result);

	return result;
}


